package Topic6.Task8;

public class MainExercise8 {
    public static void main(String[] args) {
    
    }

    class Soup1 {
        private Soup1() {
            //дозволимо створення обєктів в статичному методі
        }
    }

    static class Soup2 {
        private Soup2() {
//            створюємо один статичний обєкт
        }

        private static Soup2 ps1 = new Soup2();

        public static Soup2 access() {
            return ps1;
        }

        public void f() {
        }
    }

    public class Lunch {
        void testPrivate() {
        }
        void testStatic() {
            Soup1 soup1 = new Soup1();
        }
        void testSingleton(){
            Soup2.access().f();
        }



    }
}
