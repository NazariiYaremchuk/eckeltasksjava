package Topic6.Task6;

/**
 * Exercise 6: (1) Create a class with protected data. Create a second class in the same file
 * with a method that manipulates the protected data in the first class
 *
 * @author Nazarii Yaremchuk
 */

public class MainExercise6 {
    public static void main(String[] args) {
        new SecondPrivateClass().apple();
    }
}