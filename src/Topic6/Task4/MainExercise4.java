package Topic6.Task4;

/**
 * Exercise 4: (2) Show that protected methods have package access but are not public.
 *
 * @author Nazarii Yaremchuk
 */

public class MainExercise4 {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.setName("REX");
        System.out.println(dog.getName() + "     dog.getName");
        System.out.println("Dog " + dog.getName());
    }
}
