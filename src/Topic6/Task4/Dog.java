package Topic6.Task4;

public class Dog {
    protected String name;

    protected void setName(String name) {
        this.name = name;
    }

    protected String getName() {
        return name;
    }
}
