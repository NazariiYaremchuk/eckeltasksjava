package Topic6.Task7;

/**
 * Exercise 7: (1) Create the library according to the code fragments describing access and
 * Widget. Create a Widget in a class that is not part of the access package.
 *
 * @author Nazarii Yaremchuk
 */

public class MainExercise7 {
    public static void main(String[] args) {
        new Widget(9);
    }

    private static class Widget {
        private int a;

        public Widget(int a) {
            this.a = a;
            System.out.println(a);
        }
    }
}