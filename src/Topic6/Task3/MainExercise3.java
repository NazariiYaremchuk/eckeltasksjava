package Topic6.Task3;

import Topic6.Task3.debug.Class1;

/**
 * Exercise 3: (2) Create two packages: debug and debugoff, containing an identical class
 * with a debug( ) method. The first version displays its String argument to the console, the
 * second does nothing. Use a static import line to import the class into a test program, and
 * demonstrate the conditional compilation effect.
 *
 * @author Nazarii Yaremchuk
 */

public class MainExercise3 {
    public static void main(String[] args) {
        Class1 var = new Class1();
        var.debug("We use class-file degub.java");

        Topic6.Task3.debugoof.Class1 var1 = new Topic6.Task3.debugoof.Class1();
        var1.debug();
    }
}