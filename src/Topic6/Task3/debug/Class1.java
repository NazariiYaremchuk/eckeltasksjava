package Topic6.Task3.debug;

/**
 * Exercise 3: (2) Create two packages: debug and debugoff, containing an identical class
 * with a debug( ) method. The first version displays its String argument to the console, the
 * second does nothing. Use a static import line to import the class into a test program, and
 * demonstrate the conditional compilation effect.
 *
 * @author Nazarii Yaremchuk
 */
public class Class1 {
    public void debug(String string) {
        System.out.println(string);
    }
}