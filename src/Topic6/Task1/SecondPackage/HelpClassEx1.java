package Topic6.Task1.SecondPackage;

public class HelpClassEx1 {
    private String a;

    public HelpClassEx1() {
        System.out.println("Message about Instance of class outside of the package");

    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }
}
