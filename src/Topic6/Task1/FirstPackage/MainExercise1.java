package Topic6.Task1.FirstPackage;

import Topic6.Task1.SecondPackage.HelpClassEx1;

/**
 * Exercise 1: (1) Create a class in a package. Create an instance of your class outside of that
 * package.
 */

public class MainExercise1 {
    public static void main(String[] args) {
        HelpClassEx1 var = new HelpClassEx1();
        var.setA("Message about class in another package");
        System.out.println(var.getA());
    }
}
