package Topic6.Task5.SecondClass;

import Topic6.Task5.FirstClass;

public class SecondClass {
    public static void main(String[] args) {
        FirstClass secondClass = new FirstClass();

        System.out.println(secondClass.getVar1());
        System.out.println(secondClass.getVar2());
        System.out.println(secondClass.getVar3());

        secondClass.nazar2();
        /**
         *в цьому класі ми вже не можемо викликати метод nazar1() i nazar3() , бо nazar1() - protected
         * а nazar3() - private. Ми можемо лише викликати змінні цих типів лише через гетери
         */
    }
}
