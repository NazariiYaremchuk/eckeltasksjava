package Topic6.Task5;

/**
 * Exercise 5: (2) Create a class with public, private, protected, and package-access
 * fields and method members. Create an object of this class and see what kind of compiler
 * messages you get when you try to access all the class members. Be aware that classes in the
 * same directory are part of the “default” package.
 *
 * @author Nazarii Yaremchuk
 */

public class MainExercise5 {
    public static void main(String[] args) {
        FirstClass firstClass = new FirstClass();

        System.out.print(firstClass.getVar1() + "   " + firstClass.getVar2() + "   " + firstClass.getVar3() + "   " + firstClass.getVar4());
        System.out.print("\n" + firstClass.var1 + "   " + firstClass.var2 + "   "+firstClass.var4);

        firstClass.nazar1();
        firstClass.nazar2();
//        firstClass.nazar3();
        firstClass.nazar4();
        /**
         * Ми не можемо викликати метод nazar3() і змінна var3 - private  і маю доступ лише в межах цього класу в якому
         * він ініціалізований.
         */
    }
}
