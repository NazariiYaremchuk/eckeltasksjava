package Topic6.Task5;

public class FirstClass {
    protected String var1 = "protected field";
    public String var2 = "public field";
    private String var3 = "private field";
    String var4 = "package-access field";

    public String getVar4() {
        return var4;
    }

    public String getVar3() {
        return var3;
    }


    public String getVar1() {
        return var1;
    }

    public String getVar2() {
        return var2;
    }

    protected void nazar1() {
        System.out.println("\n" + var1);
    }

    public void nazar2() {
        System.out.println(var2);
    }

    private void nazar3() {
        System.out.println(var3);
    }

    void nazar4() {
        System.out.println(var4);
    }
}
