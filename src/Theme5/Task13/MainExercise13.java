package Theme5.Task13;

/**
 * Перевірте правильність прави із попереднього абзацу
 */

public class MainExercise13 {
    public static void main(String[] args) {
        System.out.println("Inside main()");
        Cups.cup1.f(99);// (1)
    }
}
/**
 *
 * В цій программі ми бачимо явну ініціалізацію з використанням конструкції static.Ми маємо два допоміжні класи
 * В яких ми створюємо статичні змінні і ініціалізуємо їх в блоці static.
 * /////////////////////////////////////////////////////////////////////////////////////////////////////
 * In this program, we see a clear initialization using the static design. We have two auxiliary classes
 * We create static variables and initialize them in the static block.
 */