package Theme5.Task14;

/**
 * Створіть клас з рядком static String, ініціалізованим в точці визначення, і іншим полем , ініціалізованим в блоці static
 * Добавте статичний метод,який виводить значення рядка і демонструє,що обоє рядки ініціалізовані перед використанням.
 */

public class MainExercise14 {
    public static void main(String[] args) {
        HelpClassEx14.helpFuncrionEx14();
    }
}
