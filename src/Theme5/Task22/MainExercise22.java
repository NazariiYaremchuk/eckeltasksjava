package Theme5.Task22;

import Theme5.Task21.Money;

/**
 * Напишіть команду switch для перерахунку із попереднього прикладу. Для кожного випадку вивдіть додаткову інфу про валюту
 */

public class MainExercise22 {
    public void describe(Money country) {
        switch (country) {
            case UAN:
                System.out.println("Ukraine");
                break;
            case EURO:
                System.out.println("Europe");
                break;
            case POUND:
                System.out.println("Britain");
                break;
            case RUBLE:
                System.out.println("Russia");
                break;
            case ZLOTY:
                System.out.println("Poland");
            case DOLLAR:
                System.out.println("USA");
                break;
            default:
                System.out.println("It's all :)");
        }
    }

    public static void main(String[] args) {
        MainExercise22 a = new MainExercise22();
        a.describe(Money.POUND);
        a.describe(Money.UAN);
        a.describe(Money.DOLLAR);
    }
}