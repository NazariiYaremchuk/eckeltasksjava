package Theme5.Task10;

public class HelpClassToExercise10 {
    public void finalize() {
        System.out.println("Task10 done");
    }

    public HelpClassToExercise10() {
        System.out.println("In this task method finalize() is not used ");
    }
}
