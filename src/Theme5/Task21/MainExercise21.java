package Theme5.Task21;

import java.util.Arrays;

/**
 * Створіть перерахунок назв шести паперових грошей. Переберіть результат Values() з виводом кожного значення
 */

public class MainExercise21 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(Money.values()));
    }
}
