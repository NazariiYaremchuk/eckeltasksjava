package Theme5.Task7;

/**
 * Створіть клас без конструктору. Створіть об’єкт цього класу в методі main(),
 * щоб перевірити , що конструктор за замовчуванням синтезується автоматично. *
 */

public class MainExercise7 {
    public static void main(String[] args) {
        ClassTask7 classTask7 = new ClassTask7();
        System.out.println(classTask7);
        System.out.println("Вгорі пустий конструктор");
    }
}