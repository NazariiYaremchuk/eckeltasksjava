package Theme5.Task16;

import java.util.Arrays;

/**
 * Створіть масив обєктів String . Присвойте обєкт String кожному елементу. Виведіть зміст масиву в циклі for
 */

public class MainExercise16 {
    public static void main(String[] args) {

        String[] a = {
                new String("Hello"),
                new String("world"),
        };
        String[] b = {
                new String("My name"),
                new String("is"),
                new String("Nazar"),
        };
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(b));
    }
}
