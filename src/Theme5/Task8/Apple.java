package Theme5.Task8;

/**
 * Створіть клас з двома методами. В першому методі двічі викличте інший метод:
 * один раз без ключувого слова this,а вдруге із this - просто для того , щоб переконатись в
 * працездатності цього синтаксесу; не використовуйте цей спосіб виклику на практиці
 */
public class Apple {

    void appleEat() {
        appleFruit("first");
        this.appleFruit("second");
    }

    void appleFruit(String string) {
        System.out.println("hello world    " + string);

    }


}
