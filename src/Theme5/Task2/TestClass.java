package Theme5.Task2;

public class TestClass {
    private String fieldA;
    private String fieldB;

    public TestClass(String fieldB) {
        this.fieldB = fieldB;
    }

    public void setFieldA(String fieldA) {
        this.fieldA = fieldA;
    }

    public String getFieldA() {
        return fieldA;
    }


    public String getFieldB() {
        return fieldB;
    }
}