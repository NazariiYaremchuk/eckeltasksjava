package Theme5.Task15;

/**
 * Створіть клас,похідний від String, ініціалізований в секції ініціалізації екземплярів
 */

public class MainExercise15 {
    public static void main(String[] args) {
        System.out.println("В методі main");
        new HelpClassEx15();
        System.out.println("first task done");
        new HelpClassEx15(1);
        System.out.println("second task done");
    }
}