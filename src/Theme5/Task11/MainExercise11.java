package Theme5.Task11;

import Theme5.Task10.HelpClassToExercise10;

/**
 * Переробіть попередню вправу так , щоб метод finalizze() обовязково був використаний
 */

public class MainExercise11 {
    public static void main(String[] args) {
        HelpClassToExercise10 var = new HelpClassToExercise10();
        var.finalize();
    }
}
